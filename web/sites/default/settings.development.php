<?php
$databases['default']['default'] = array (
  'database' => '',
  'username' => '',
  'password' => '',
  'prefix' => '',
  'host' => '127.0.0.1',
  'port' => 3306,
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['hash_salt'] = '18789194925909ea1f022307.50273308';

// File paths.
$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = '/Volumes/webdev/www/digipolis/drupal8_site_gentse-feesten/local/files/private';
$config['system.file']['path']['temporary'] = '/tmp';
$config['system.file']['path']['temporary'] = '/tmp';

// Config.
$config_directories['sync'] = '../config/sync';

// Install profile.
$settings['install_profile'] = 'minimal';

// Disable shield module.
$config['shield.settings']['user'] = '';
$config['shield.settings']['pass'] = '';

// GA.
$config['google_analytics.settings']['account'] = '';

// Gent services.
$config['dg_services.settings']['endpoint'] = 'http://services.web.test.gentgrp.gent.be';
$config['dg_services.settings']['token'] = '035aa2ea78975b51179b25e9d67b4187830b612e';
$config['dg_services.settings']['secret'] = 'd197c687e38e10331ff6da62bf19888dadadf5aa';
$config['dg_services.settings']['version'] = 1;

// URL to sync event data.
$settings['gf_sync_url'] = 'http://gf-invoer.web.test.gentgrp.gent.be/api/v1/';

// SOLR config.
$config['search_api.server.solr'] = [
  'backend_config' => [
    //'connector' => 'basic_auth',
    'connector_config' => [
      'host' => '127.0.0.1',
      'path' => '/solr',
      'core' => 'digi_gf17',
      'port' => '8983',
      //'username' => '',
      //'password' => '',
    ],
  ],
];


// Varnish cache purger.
//$config['purge_purger_http.settings.5c61093258']['hostname'] = '10.25.2.107';
//$config['purge_purger_http.settings.5c61093258']['headers'][1]['value'] = 'gf17.web.test.gentgrp.gent.be';

// Flickr settings.
$settings['gf_picture_flickr_api_key'] = '935d615acb6a91a69eca0a3f9a2eab4c';
$settings['gf_picture_flickr_photoset_id'] = '72157671088642381';

// Youtube settings.
$settings['gf_video_youtube_api_key'] = 'AIzaSyBUwKf1ePHkCL9oc0ENC5NhDwM3izcqDgs';
$settings['gf_video_youtube_channel'] = 'UCNWkhbIX0Ta8DnlvPra4Q3w';





/****************************************************************************
/* Show all errors the PHP way.
 ****************************************************************************/

// Set the default error reporting level (should be TRUE during development).
$drupal_debug = TRUE;
$drupal_debug_forced = TRUE;

// Change error reporting based on the "drupal_debug" get variable.
if (isset($_GET['drupal_debug'])) {
  $drupal_debug = (bool) $_GET['drupal_debug'];
}

// Change error reporting behaviour except when running drush.
if ($drupal_debug_forced || ($drupal_debug && php_sapi_name() != 'cli')) {
  // Drupal error reporting level (production = 2).
  $conf['error_level'] = 0;

  // PHP error reporting > ALL except STRICT.
  error_reporting(E_ALL ^ E_STRICT);

  // Show all errors on screen.
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

  // Don't let Drupal handle errors & exceptions.
  restore_error_handler();
  restore_exception_handler();
}

/**
 * Theme debug.
 */
$drupal_theme_debug = FALSE;
$drupal_theme_debug_forced = TRUE;
if (isset($_GET['drupal_theme_debug'])) {
  $drupal_theme_debug = (bool) $_GET['drupal_theme_debug'];
}
if ($drupal_theme_debug || $drupal_theme_debug_forced) {
  $config['theme_debug'] = TRUE;
  $config['system.performance']['css']['preprocess'] = FALSE;
  $config['system.performance']['js']['preprocess'] = FALSE;
  $settings['cache']['bins']['render'] = 'cache.backend.null';
  $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
}



/**
 * Enable local development services.
 */
//$settings['container_yamls'][] = DRUPAL_ROOT . '/../local/development.services.yml';
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
//$settings['container_yamls'][] = $app_root . '/' . $site_path . '/development.services.yml';

